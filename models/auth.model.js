
const Sequelize = require('sequelize');
const sequelize = require('../config/database');

const User = sequelize.define("users", {
    id: {
        type: Sequelize.STRING,
        defaultValue: Sequelize.UUIDV4,
        primaryKey: true,
        allowNull: false,
    },
    fullname: {
        type: Sequelize.TEXT,
        allowNull: false,
    },
    email: {
        type: Sequelize.TEXT,
        allowNull: true,
        unique: true,
        validate: { isEmail: true },
    },
    address: {
        type: Sequelize.TEXT,
        allowNull: false,
    },
    birthday: {
        type: Sequelize.TEXT,
        allowNull: false,
    },
    phone: {
        type: Sequelize.TEXT,
        allowNull: false,
    },
    password: {
        type: Sequelize.TEXT,
        allowNull: true,
    }
}, {
    timestamps: true
})

module.exports = User;