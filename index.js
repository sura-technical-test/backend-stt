const express = require('express');
const app = express();
const winston = require('./config/winston');
const bodyParser = require('body-parser');
const morgan = require('morgan');
const sequelize = require('./config/database');
const cors = require('cors');
sequelize.sync();

// Configurations
const port = process.env.PORT || 5000;

// Import Routes
const authRoute = require('./routes/auth.routes');
const userRoute = require('./routes/users.routes');
const { post } = require('./routes/users.routes');


// Middlewares
app.use(morgan("combined", { stream: winston.stream }));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }))

app.use(cors({
    origin: ['http://localhost:4200', 'https://sura-tt-fcob-frontend.herokuapp.com'],
    methods: '*',
    allowedHeaders: ['Content-Type', 'token'],
}))

// Routes
app.use('/api/v1/auth', authRoute);
app.use('/api/v1/users', userRoute);

app.listen(port, () => {
    winston.info(`Server on port ${port}`)
})