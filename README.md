# Backend para Prueba Tecnica Sura

### Librerias empleadas 
* app-root-path: para determinar el root de la aplicacion
* bcryptjs: para crear encryptaciones asyncronas y no
* body-parser: para analizar los cuerpos de las peticiones 
* cors: para habilitarlos origines de las peticion y los methodos entre otros 
* dotenv: para usar las variables de entorno de la aplicacion
* express: Framework web para NODEJS
* jsonwebtoken: para la generacion de token
* morgan: logger de peticion HTTP
* sequelize: ORM de postgres, mysql, mariaDB
* winston: Para añadir los logs en un archivo
* nodemon: Reinicia automaticamente el servidor en desarrollo

### Desplegar en un servidor de desarrollo

* Crear la imagen y subirla a los servidores de heroku

    ```
    heroku container:push web -a sura-tt-fcob-backend
    ```

* Publicar la imagen en los servidores 
    ```
    heroku container:release web -a sura-tt-fcob-backend
    ```

### Correr la aplicacion en local
* Crear una base de datos en Postgres
* Setear los valores en un archivo .env, seguir el ejemplo del .env.default
* Instalar las dependencias del repositorio

    ```
    npm install
    ```

* Correr con Nodemon 
    ```
    npm run dev
    o
    nodemon index.js
    ```

* Correr sin Nodemon 
    ```
    npm run start
    o
    node index.js
    ```

### Rutas

* Iniciar sesion
    ```
    curl --location --request POST 'localhost:5000/api/v1/auth/signin' --header 'Content-Type: application/json' --data-raw 
    '{
        "email": "franciscobrioneslavados@gmail.com", 
        "password": "123456"
    }'
    ```
* Registrar un usuario
    ```
    curl --location --request POST 'localhost:5000/api/v1/auth/signup' --header 'Content-Type: application/json' --data-raw 
    '{
        "fullname": "Francisco Briones Lavados",
        "email": "franciscobrioneslavados@gmail.com",
        "address": "Berlin 927",
        "birthday": "11/01/1988",
        "phone": "951994389",
        "password": "123456"
    }'
    ```
* List de Usuarios
    ```
    curl --location --request GET 'localhost:5000/api/v1/users'
    ```
