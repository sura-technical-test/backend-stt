const Sequelize = require('sequelize');
require('dotenv').config();



const sequelize = new Sequelize(
    process.env.DB_DATABASE,
    process.env.DB_USER,
    process.env.DB_PASSWORD,
    {
        host: process.env.BD_HOST,
        port: process.env.DB_PORT,
        dialect: "postgres",
        pool: {
            max: 5,
            min: 0,
            require: 30000,
            idle: 10000,
        },
        logging: false,
    }
)

module.exports = sequelize;