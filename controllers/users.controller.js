
const User = require('../models/auth.model');

exports.getAll = async (req, res, next) => {
    try {
        const listUsers = await User.findAll();
        if (listUsers) {
            res.status(200).json({
                message: "List users",
                listUsers: listUsers,
            })
        }
    } catch (e) {
        winston.info(e.toString());
        res.status(500).json({
            system_message: e.toString()
        })
    }
}