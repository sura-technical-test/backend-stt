

const User = require('../models/auth.model');
const winston = require('../config/winston');
const jwt = require('jsonwebtoken');
const bcrypt = require('bcryptjs');
require('dotenv').config();

/**
 * Register User
 * 
 * @param {*} req 
 * @param {*} res 
 * @param {*} next 
 */
exports.signUp = async (req, res, next) => {

    // Hashear la contraseña 
    const userBody = {
        fullname: req.body.fullname,
        email: req.body.email,
        address: req.body.address,
        birthday: req.body.birthday,
        phone: req.body.phone,
        password: bcrypt.hashSync(req.body.password)
    }

    try {

        const foundUser = await User.findOne({
            where: {
                email: userBody.email
            }
        });

        if (foundUser) {
            return res.status(300).json({
                message: 'The account entered already exists!'
            })
        } else {
            const newUser = await User.create(userBody, {
                fields: ["fullname", "email", "address", "birthday", "phone", "password"]
            });

            if (newUser) {
            
                return res.status(201).json({
                    message: "User Saved!",
                })
            } else {
                return res.status(500).json({
                    message: "There was a problem saving to the database",
                })
            }
        }


    } catch (e) {
        winston.info(e.toString());
        res.status(500).json({
            system_message: e.toString()
        })
    }
}

/**
 * Login  User
 * @param {*} req 
 * @param {*} res 
 * @param {*} next 
 */
exports.signIn = async (req, res, next) => {

    const userBody = {
        email: req.body.email,
        password: req.body.password
    }

    try {
        const foundUser = await User.findOne({
            where: {
                email: userBody.email
            }
        });

        if(!foundUser){
            return res.status(301).json({
                message: 'The account entered does not exist'
            })
        }else {

            const validpassword = bcrypt.compareSync(
                userBody.password,
                foundUser.password
            )
            if(validpassword){
                const secret = process.env.JWT_SECRET;
                const acccessToken = await jwt.sign({ id: foundUser.id }, secret, {
                    expiresIn: 50000
                });
    
                return res.status(200).json({
                    message: "Logged success!",
                    token: acccessToken,
                    userId: foundUser['id']
                })
            }else {
                return res.status(301).json({
                    message: "the password does not belong to the registered account!"
                })
            }

            
        }


    } catch (e) {
        winston.info(e.toString());
        res.status(500).json({
            system_message: e.toString()
        })
    }


}